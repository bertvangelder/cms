<?php
/**
 * Created by PhpStorm.
 * User: Bert
 * Date: 2/12/2016
 * Time: 15:24
 */

namespace Custom\CMSBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', array(
            'error'         => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     *
     */
    public function logoutAction()
    {
    }
}