<?php

namespace Custom\CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     *
     * @Route("/", name="custom_cms_homepage")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pages = $em->getRepository('CustomCMSBundle:Page')->findAll();
        return $this->render('default/index.html.twig', array(
            'pages'=>$pages
        ));
    }

    /**
     * @Route("/page/{id}", name="custom_cms_page_display")
     */
    public function displayAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('CustomCMSBundle:Page')->find($id);
        return $this->render('default/display.html.twig', array(
           'page' => $page
        ));
    }
}
